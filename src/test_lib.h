/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_lib.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjukmong <tjukmong@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/31 21:44:08 by tjukmong          #+#    #+#             */
/*   Updated: 2022/11/01 07:45:21 by tjukmong         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEST_LIB_H
# define TEST_LIB_H
# include <avr/io.h>
# include <util/delay.h>
# include <stdio.h>

# define BAUDRATE	115200

void	hello_world(void);

#endif
