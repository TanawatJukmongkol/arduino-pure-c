# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tjukmong <tjukmong@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/10/27 23:18:09 by tjukmong          #+#    #+#              #
#    Updated: 2022/11/01 08:16:03 by tjukmong         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME		= hello_world

CHIP		= atmega328p
PORT		= /dev/ttyACM0
BAUDRATE	= 115200

SRC_DIR		= ./src/
BUILD_DIR	= ./build/

SRC			= Hello_Arduino.c test_lib.c
OBJ			= ${SRC:.c=.o}
HEX			= ${addprefix ${NAME},.hex}

OBJ_PATH	= ${addprefix ${BUILD_DIR},${OBJ}}

all: init bin flash

init:
	mkdir -p ${BUILD_DIR}

${BUILD_DIR}%.o: ${SRC_DIR}%.c
	avr-gcc -Os -DF_CPU=16000000UL -mmcu=${CHIP} -c -o $@ $^

bin: ${BUILD_DIR} ${NAME}

${NAME}: ${OBJ_PATH}
	avr-gcc -mmcu=${CHIP} ${OBJ_PATH} -o ${NAME}
	avr-objcopy -O ihex -R .eeprom ${NAME} ${HEX}

flash: ${HEX}
	avrdude -F -V -c arduino -p $(shell echo "${CHIP}" | tr '[:lower:]' '[:upper:]') -P ${PORT} -b ${BAUDRATE} -U flash:w:${HEX}

clean:
	rm -rf ${BUILD_DIR}

fclean: clean
	rm -f ${HEX}
	rm -f ${NAME}

re: fclean all

.PHONY: all bin flash clean fclean re